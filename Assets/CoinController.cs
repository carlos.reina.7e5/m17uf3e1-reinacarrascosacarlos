using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        Debug.Log("Has tocado la moneda");
        Destroy(gameObject);
        GameManager.CoinCount++;
        Debug.Log("Monedas recogidas: " + GameManager.CoinCount);
    }
}
