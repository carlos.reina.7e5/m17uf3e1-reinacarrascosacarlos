using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int CoinCount;

    // Update is called once per frame
    void Update()
    {
        if (CoinCount >= 10)
        {
            SceneManager.LoadScene("Playground");
            CoinCount = 0;
        } 
            
    }
}
