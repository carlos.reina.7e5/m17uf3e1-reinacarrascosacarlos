using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    private Text _coinCount;

    // Start is called before the first frame update
    void Start()
    {
        _coinCount = GameObject.Find("CoinCounter").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _coinCount.text = "Coins : " + GameManager.CoinCount + " (10)";
    }
}
